@section('title') Edit Company @endsection
@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;margin-top: 10%;">
        @include('users.layout.message')
        <a href="{{route('logout')}}" class="btn btn-info btn-md active" role="button" aria-pressed="true" style="float:right;">Logout</a>

        <h2>Dashboard</h2>

        <h1 style="margin-left: 25%;">Edit Company {{$company->company_name}}</h1>
        <a href="{{route('dashboard')}}" class="btn btn-primary btn-md active" role="button" aria-pressed="true" >Go Back</a>

        <div style="width:65%;margin-left: 25%">
            <form method="POST" action="{{route('company.update',$company->id)}}">
                @csrf
                <div class="form-group">
                    <label for="company_name">Company Name</label>
                    <input type="text" name="company_name" class="form-control" id="company_name" aria-describedby="company_nameHelp" value="{{$company->company_name}}">
                    @error('company_name')
                    <div style="color:red;">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="pib">PIB</label>
                    <input type="text" name="pib" class="form-control" id="pib" aria-describedby="pibHelp" value="{{$company->pib}}">
                    @error('pib')
                    <div style="color:red;">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="street" class="form-control" id="street" aria-describedby="streetHelp" value="{{$company->street}}">
                            @error('street')
                            <div style="color:red;">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-2">
                            <input type="text" name="number" class="form-control" id="number" aria-describedby="numberHelp" value="{{$company->number}}">
                            @error('number')
                            <div style="color:red;">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="city" class="form-control" id="city" aria-describedby="cityHelp" value="{{$company->city}}">
                            @error('city')
                            <div style="color:red;">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label for="contact_person">Contact Person</label>
                    <input type="text" name="contact_person" class="form-control" id="contact_person" aria-describedby="contact_personHelp" value="{{$company->contact_person}}">
                    @error('contact_person')
                    <div style="color:red;">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" value="{{$company->email}}">

                    @error('email')
                    <div style="color:red;">{{ $message }}</div>
                    @enderror

                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="number" name="phone" class="form-control" id="phone" aria-describedby="phoneHelp" value="{{$company->phone}}">
                    @error('phone')
                    <div style="color:red;">{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>



    </div>
@endsection


