@section('title') On Hold Service @endsection
@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;margin-top: 10%;">
        @include('users.layout.message')
        <a href="{{route('dashboard')}}" class="btn btn-secondary btn-md " role="button" aria-pressed="true" style="float:right;margin-left: 10px;">Go to Companies</a>
        <a href="{{route('logout')}}" class="btn btn-info btn-md active" role="button" aria-pressed="true" style="float:right;">Logout</a>

        <h2>Dashboard</h2>

        <h1 style="text-align: center;">On Hold</h1>
            <hr>
        @include('users.layout.topNavMenu')

        <div class="row">
            @include('users.layout.table_ajax')
        </div>

    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('service.onHold') }}",
                columns: [
                    {data: 'id', name: 'services.id'},
                    {data: 'name', name: 'users.name'},
                    {data: 'equipment', name: 'services.equipment'},
                    {data: 'serial_no', name: 'services.serial_no'},
                    {data: 'note', name: 'services.note'},
                    {data: 'created_at', name: 'services.created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]
            });
        });

    </script>
@endsection


