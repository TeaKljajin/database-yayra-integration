@section('title') Taken Services @endsection
@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;margin-top: 10%;">
        @include('users.layout.message')
        <a href="{{route('dashboard')}}" class="btn btn-secondary btn-md " role="button" aria-pressed="true" style="float:right;margin-left: 10px;">Go to Companies</a>
        <a href="{{route('logout')}}" class="btn btn-info btn-md active" role="button" aria-pressed="true" style="float:right;">Logout</a>

        <h2>Dashboard</h2>

        <h1 style="text-align: center;">Taken</h1>
            <hr>
        @include('users.layout.topNavMenu')
        <div class="row">
            <div class="col-md-12">
                <br>
                <table class="table table-bordered data-table mt-5">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th>User</th>
                        <th>Equipment</th>
                        <th>Serial No</th>
                        <th>Note</th>
                        <th>Created At</th>
                        <th>Taken At</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('service.taken') }}",
                columns: [
                    {data: 'id', name: 'services.id'},
                    {data: 'name', name: 'users.name'},
                    {data: 'equipment', name: 'services.equipment'},
                    {data: 'serial_no', name: 'services.serial_no'},
                    {data: 'note', name: 'services.note'},
                    {data: 'created_at', name: 'services.created_at'},
                    {data: 'updated_at', name: 'services.updated_at'},

                ]
            });
        });

    </script>
@endsection


