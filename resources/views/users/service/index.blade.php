@section('title') Add New Service @endsection
@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;margin-top: 10%;">
        @include('users.layout.message')

        <a href="{{route('dashboard')}}" class="btn btn-secondary btn-md " role="button" aria-pressed="true" style="float:right;margin-left: 10px;">Go to Companies</a>
        <a href="{{route('logout')}}" class="btn btn-info btn-md active" role="button" aria-pressed="true" style="float:right;">Logout</a>

            <h2>Welcome To Service</h2>
            <h1 style="text-align: center;">Create new Service</h1>
            <button style="float:right" type="button" name="create_record" id="create_record" class="btn btn-outline-success btn-sm">Create New User</button>

       <div class="row mt-5">
           <div class="col-md-2 mt-5">
               <a href="#" class="btn btn-outline-primary btn-lg active" role="button" aria-pressed="true" style="width: 100%;">Form</a><hr>
               <a href="{{route('service.accepted')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Accepted</a><hr>
               <a href="{{route('service.inProgress')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">In progress</a><hr>
               <a href="{{route('service.onHold')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">On hold</a><hr>
               <a href="{{route('service.finished')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Finished</a><hr>
               <a href="{{route('service.taken')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Taken </a>

           </div>
           <div class="col-md-10">
               <div style="width:65%;margin-left: 15%">
                   <form method="POST" action="{{route('service.store')}}">
                       @csrf
                       <input type="hidden" name="status">
                       <div class="form-group">
                           <select class="form-control" name="user_id">
                               <option>Select User</option>
                               @foreach ($users as $user)
                                   <option value="{{ $user->id }}"> {{ $user->name }} </option>
                               @endforeach
                           </select>
                           @error('user_id')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>

                       <div class="form-group">
                           <label for="date">Date</label>
                           <input type="date" name="date" class="form-control" id="date" aria-describedby="dateHelp">
                           @error('date')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>

                       <div class="form-group">
                           <label for="equipment">Equipment</label>
                           <textarea name="equipment" class="form-control" id="equipment" aria-describedby="equipmentHelp" rows="4" cols="50"  placeholder="Description of equipment received for service"></textarea>
                           @error('equipment')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>
                       <div class="form-group">
                           <label for="serial_no">Serial No</label>
                           <input type="number" name="serial_no" class="form-control" id="serial_no" aria-describedby="serial_noHelp" placeholder="Enter serial number">
                           @error('serial_no')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>
                       <div class="form-group">
                           <label for="note">Note</label>
                           <textarea name="note" class="form-control" rows="4" cols="50" id="note" aria-describedby="noteHelp" placeholder="Note related to the problem with the brought equipment"></textarea>
                           @error('note')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>
                       <button type="submit" class="btn btn-primary">Submit</button>
                   </form>

                   <!--------Modal => Create New User --------->
                   <div id="formModal" class="modal fade" role="dialog">
                       <div class="modal-dialog">
                           <div class="modal-content">
                               <div class="modal-header">
                                   <h4 class="modal-title">Add New User</h4>
                                   <button type="button" class="close" data-dismiss="modal">&times</button>
                               </div>
                               <div class="modal-body">
                                   <span id="form_result"></span>
                                   <form method="post" id="sample_form" class="form-horizontal">
                                       @csrf
                                       <div class="form-group">
                                           <label class="control-label col-md-4" for="name">Name</label>
                                           <div class="col-md-8">
                                               <input type="text" name="name" id="name" class="form-control"/>
                                               <span class="error-text name_error" style="color:red"></span>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <label class="control-label col-md-4" for="email">Email</label>
                                           <div class="col-md-8">
                                               <input type="email" name="email" id="email" class="form-control"/>
                                               <span class="error-text email_error" style="color:red"></span>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <label class="control-label col-md-4" for="password">Password</label>
                                           <div class="col-md-8">
                                               <input type="password" name="password" id="password" class="form-control"/>
                                               <span class="error-text password_error" style="color:red"></span>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <input type="hidden" name="action" id="action" value="Add"/>
                                           <input type="submit" name="action_button" class="btn btn-success" value="Add"/>
                                       </div>
                                   </form>

                               </div>
                           </div>
                       </div>
                   </div>


               </div>
           </div>
       </div>
    </div>

@endsection
@section('script')
<script>
    $(document).ready(function(){
          $('#user_table').DataTable({
              processing:true,
              serverSide:true,
              ajax:{
                  url:"{{route('modalStore')}}",
              },
              columns:[
                  {
                      data: 'name',
                      name: 'name',
                  },
                  {
                      data: 'email',
                      name: 'email',
                  },
                  {
                      data: 'password',
                      name: 'password',
                  },
                  {
                      data: 'action',
                      name: 'action',
                      orderable:false
                  }
              ]
          });
          $('#create_record').click(function(){
               $('#formModal').modal('show');
          });
          $('#sample_form').on('submit',function(event){
                event.preventDefault();
                var action_url = '';
                if($('#action').val() == 'Add'){
                     action_url = "{{route('modalStore')}}";
                }
                $.ajax({
                    url: action_url,
                    method:'POST',
                    data:$(this).serialize(),
                    dataType:'json',
                    beforeSend:function() {
                        $(document).find('span.error-text').text('');
                    },
                    success:function(data){
                        var html='';
                        if(data.errors){
                            $.each(data.errors,function(prefix, val){
                                $('span.' + prefix + '_error').text(val[0]);
                            });
                        }
                        if(data.success){
                            html = '<div class="alert alert-success">' + data.success + '</div>';
                            $('#sample_form')[0].reset();
                            $('#form_result').html(html);
                        }
                    }
              });
          })
    })

</script>
@endsection
