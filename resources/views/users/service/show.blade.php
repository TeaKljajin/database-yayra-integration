@section('title') Show Service @endsection
@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;margin-top: 10%;">
        @include('users.layout.message')
        <a href="{{route('dashboard')}}" class="btn btn-secondary btn-md " role="button" aria-pressed="true" style="float:right;margin-left: 10px;">Go to Companies</a>
        <a href="{{route('logout')}}" class="btn btn-info btn-md active" role="button" aria-pressed="true" style="float:right;">Logout</a>

        <h2>Dashboard</h2>

        <h1 style="text-align: center;">Service Serial No: {{$service->serial_no}}</h1>

        <div class="row mt-5">
            <div class="col-md-2 mt-5">
                <a href="{{route('service.index')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Form</a><hr>
                <a href="{{route('service.accepted')}}" class="btn btn-outline-primary btn-lg active" role="button" aria-pressed="true" style="width: 100%;">Accepted</a><hr>
                <a href="{{route('service.inProgress')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">In progress</a><hr>
                <a href="{{route('service.onHold')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">On hold</a><hr>
                <a href="{{route('service.finished')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Finished</a><hr>
                <a href="{{route('service.taken')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Taken </a>
            </div>
            <div class="col-md-10">
              <h4>User :  {{$service->user->name}}</h4>
                <h4>Equipment</h4>
                <div style="border:2px solid lightgrey;height:150px;width:100%;padding:1%;">{{$service->equipment}}</div><br>
                <h4>Note</h4>
                <div style="border:2px solid lightgrey;height:150px;width:100%;padding:1%;">{{$service->note}}</div><br>
                <h4>Created At : {{$service->created_at->diffForHumans()}}</h4><br>

                <a href="{{route('service.accepted')}}" class="btn btn-small btn-primary">Go Back</a>
                @if($service->status === 1)
                    <a href="{{route('service.serviceActive',$service->id)}}" class="btn btn-small btn-outline-success">Service</a>
                @else
                    <a href="{{route('service.serviceNotActive',$service->id)}}" class="btn btn-small btn-success">Service</a>
                @endif
                @if($service->status === 1 || $service->status === 2)
                    <a href="{{route('service.Finished',$service->id)}}" class="btn btn-small btn-outline-info">Finished</a>
                @else
                    <a href="{{route('service.notFinished',$service->id)}}" class="btn btn-small btn-info">Finished</a>
                @endif
                @if($service->status === 1 || $service->status === 2 || $service->status === 3 )
                    <a href="{{route('service.serviceTaken',$service->id)}}" class="btn btn-small btn-outline-info">Taken</a>
                @else
                    <a href="{{route('service.serviceNotTaken',$service->id)}}" class="btn btn-small btn-info">Taken</a>
                @endif
                <a href="{{route('service.show',$service->id)}}" class="btn btn-small btn-secondary">Show</a>
                <a href="{{route('service.delete',$service->id)}}" class="btn btn-small btn-danger" onclick="return confirm('Are you sure to delete?')" >Delete</a>
            </div>
        </div>
    </div>
@endsection



