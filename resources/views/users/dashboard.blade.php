@section('title') All Companies @endsection
@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;margin-top: 10%;">
        @include('users.layout.message')
            <a href="{{route('service.index')}}" class="btn btn-secondary btn-md " role="button" aria-pressed="true" style="float:right;margin-left: 10px;">Go to Service</a>
            <a href="{{route('logout')}}" class="btn btn-info btn-md active" role="button" aria-pressed="true" style="float:right;">Logout</a>

            <h2>Dashboard</h2>

                <h1 style="text-align: center;">Welcome {{Auth::user()->name}}</h1>
            <a href="{{route('company.create')}}" class="btn btn-outline-success btn-md " role="button" aria-pressed="true" >Create New Company</a>
            <hr>
            <br>
            <table class="table table-bordered data-table mt-5">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th>Company</th>
                    <th>PIB</th>
                    <th>Street</th>
                    <th>Nu</th>
                    <th>City</th>
                    <th>Contact</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('dashboard') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'company_name', name: 'company_name'},
                    {data: 'pib', name: 'pib'},
                    {data: 'street', name: 'street'},
                    {data: 'number', name: 'number'},
                    {data: 'city', name: 'city'},
                    {data: 'contact_person', name: 'contact_person'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]
            });
        });

    </script>
@endsection


