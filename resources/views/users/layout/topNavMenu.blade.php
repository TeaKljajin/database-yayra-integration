<div>
    <a href="{{route('service.index')}}" class="btn btn-outline-primary btn-lg {{ request()->routeIs('service.index') ? 'active' : '' }}" role="button" aria-pressed="true" style="width:16%">Form</a>
    <a href="{{route('service.accepted')}}" class="btn btn-outline-primary btn-lg {{ request()->routeIs('service.accepted') ? 'active' : '' }}" role="button" aria-pressed="true" style="width:16%">Accepted</a>
    <a href="{{route('service.inProgress')}}" class="btn btn-outline-primary btn-lg {{ request()->routeIs('service.inProgress') ? 'active' : '' }}" role="button" aria-pressed="true" style="width:16%">In progress</a>
    <a href="{{route('service.onHold')}}" class="btn btn-outline-primary btn-lg {{ request()->routeIs('service.onHold') ? 'active' : '' }}" role="button" aria-pressed="true" style="width:16%">On hold</a>
    <a href="{{route('service.finished')}}" class="btn btn-outline-primary btn-lg {{ request()->routeIs('service.finished')? 'active' : '' }}" role="button" aria-pressed="true" style="width:16%">Finished</a>
    <a href="{{route('service.taken')}}" class="btn btn-outline-primary btn-lg {{ request()->routeIs('service.taken') ? 'active' : '' }}" role="button" aria-pressed="true" style="width:16%">Taken </a>
</div><hr>
