@include('front.layout.head')
<body class="antialiased">
<div class="container">
    @yield('content')

</div>
{{--@include('front.layout.footer')--}}
    @include('front.layout.scriptsJS')
    @yield('script')
</body>
</html>

