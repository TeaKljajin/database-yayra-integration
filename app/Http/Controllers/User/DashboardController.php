<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Company;
use Yajra\DataTables\Facades\DataTables;

class DashboardController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }

//    public function index(){
//        $companies = Company::latest()->paginate(2);
//        return view('users.dashboard',compact('companies'));
//    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $companies = Company::select('*');
            return Datatables::of($companies)
                ->addIndexColumn()
                ->addColumn('action', function($company){
                    $btn = '<a href="/company/edit/'.$company->id.'" class="edit btn btn-primary btn-sm" style="margin-right: 4px;">Edit</a>';
                    $btn = $btn.'<a href="/company/delete/'.$company->id.'" class="edit btn btn-danger btn-sm" onclick="return confirm(`Are you sure you wana delete`);">Delete</a>';

                    return $btn;
                })

                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users.dashboard');
    }




    public function logout(){
        Auth::logout();
        //Session message
        session()->flash('success','You logout successfully');
        //Redirect to
        return redirect('/');

    }
}
