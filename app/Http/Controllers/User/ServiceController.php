<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;


class ServiceController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }


    public function index(){
        $users = User::all();
        return view('users.service.index',compact('users'));
    }


    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id'=> 'required|integer',
            'date' => 'required|date_format:Y-m-d|before:tomorrow',
            'equipment' => 'required|string|max:255',
            'serial_no' => 'nullable|digits_between:2,10',
            'note' => 'required|string|max:255',
        ],[
            'user_id.integer' => 'You must choose user',
            'serial_no.digits_between' => 'Serial number must be between 2 and 10 digits'
        ]);

        if ($validator->fails()) {
            return redirect()->route('service.index')
                ->withErrors($validator)
                ->withInput();
        } else {

            Service::insert([
                'user_id' => $request->user_id,
                'created_at' => $request->date,
                'equipment' => $request->equipment,
                'serial_no' => $request->serial_no,
                'note' => $request->note,
                'status' => 1,

            ]);
            //Session message
            session()->flash('success', 'You created successfully new service');
            //Redirect to
            return redirect()->route('service.accepted');
        }
    }


    public function accepted(Request $request)
    {
        if ($request->ajax()) {
            $services = Service::join('users', 'services.user_id', '=', 'users.id')
                ->select(['services.id', 'users.name', 'services.equipment', 'services.serial_no', 'services.note', 'services.created_at','services.status']);

            return Datatables::of($services)
                ->addIndexColumn()
                ->editColumn('equipment', function($service){
                    return Str::limit($service->equipment, 10);
                })
                ->editColumn('note', function($service){
                    return Str::limit($service->note, 4);
                })
                ->editColumn('created_at', function ($service) {
                    return $service->created_at->format('Y/m/d');
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
                })
                ->addColumn('action', function($service){
                    $btn = '<a href="/service/accepted/show/'.$service->id.'" class="edit btn btn-info btn-sm" style="margin-right: 4px;">Show</a>';
                    if($service->status === 1){
                        $btn = $btn. '<a href="/service/accepted/serviceActive/'.$service->id.'" class="edit btn btn-outline-success btn-sm" style="margin-right: 4px;">Service</a>';
                    } else{
                        $btn = $btn.'<a href="/service/accepted/serviceNotActive/'.$service->id.'" class="edit btn btn-success btn-sm" style="margin-right: 4px;">Service</a>';
                    }
                    if($service->status === 1 || $service->status === 2){
                        $btn = $btn.'<a href="/service/accepted/finished/'.$service->id.'" class="edit btn btn-outline-secondary btn-sm" style="margin-right: 4px;">Finished</a>';
                    }else{
                        $btn = $btn.'<a href="/service/accepted/notFinished/'.$service->id.'" class="edit btn btn-secondary btn-sm" style="margin-right: 4px;">Finished</a>';
                    }
                    if($service->status === 1 || $service->status === 2 || $service->status === 3 ){
                        $btn = $btn.'<a href="/service/accepted/taken/'.$service->id.'" class="edit btn btn-outline-warning btn-sm" style="margin-right: 4px;">Taken</a>';
                    }else{
                        $btn = $btn.'<a href="service/accepted/notTaken/'.$service->id.'" class="edit btn btn-warning btn-sm" style="margin-right: 4px;">Taken</a>';
                    }
                    $btn = $btn.'<a href="/service/accepted/delete/'.$service->id.'" class="edit btn btn-danger btn-sm" onclick="return confirm(`Are you sure you wana delete`);">Delete</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users.service.accepted');
    }

    public function serviceTaken($id){
        $service = Service::find($id);
        $service->update([
            'status' => 4
        ]);
        //Session message
        session()->flash('success', 'You successfully taken service');
        //Redirect to
        return redirect()->route('service.accepted');
    }

    public function serviceNotTaken($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You restored successfully service from taken');
        //Redirect to
        return redirect()->route('service.accepted');
    }


    public function delete($id){
        //Find Service
        $service = Service::find($id);
        $service->delete();
        //Session message
        session()->flash('success', 'You deleted successfully service');
        //Redirect to
        return redirect()->route('service.accepted');
    }


    public function serviceActive($id){
        $service = Service::find($id);
        $service->update([
            'status' => 2
        ]);
        //Session message
        session()->flash('success', 'You push successfully on service');
        //Redirect to
        return redirect()->route('service.accepted');
    }

    public function serviceNotActive($id){
        $service = Service::find($id);
        $service->update([
            'status' => 1
        ]);
        //Session message
        session()->flash('success', 'You restore successfully from services');
        //Redirect to
        return redirect()->route('service.accepted');
    }


    public function serviceFinished($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You successfully finished service');
        //Redirect to
        return redirect()->route('service.accepted');
    }

    public function serviceNotFinished($id){
        $service = Service::find($id);
        $service->update([
            'status' => 2
        ]);
        //Session message
        session()->flash('success', 'You restored successfully service from finished');
        //Redirect to
        return redirect()->route('service.accepted');
    }

    public function inProgress(Request $request){
        if ($request->ajax()) {
            $services = Service::join('users', 'services.user_id', '=', 'users.id')
                ->select(['services.id', 'users.name', 'services.equipment', 'services.serial_no', 'services.note', 'services.created_at'])->where('status',2);
            return Datatables::of($services)
                ->addIndexColumn()
                ->editColumn('equipment', function($service){
                    return Str::limit($service->equipment, 30);
                })
                ->editColumn('note', function($service){
                    return Str::limit($service->note, 10);
                })
                ->editColumn('created_at', function ($service) {
                    return $service->created_at->format('Y/m/d');
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
                })
                ->addColumn('action', function($service){
                    $btn = '<a href="/service/inProgress/serviceNotActive/'.$service->id.'" class="edit btn btn-outline-success btn-sm" style="margin-right: 4px;">On Hold</a>';
                    $btn = $btn.'<a href="/service/inProgress/serviceFinished/'.$service->id.'" class="edit btn btn-outline-secondary btn-sm">Finished</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users.service.inProgress');
    }


    public function inProgressNotActive($id){
        $service = Service::find($id);
        $service->update([
            'status' => 1
        ]);
        //Session message
        session()->flash('success', 'You restore successfully from services');
        //Redirect to
        return redirect()->route('service.inProgress');
    }

    public function inProgressFinished($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You successfully finished service on service');
        //Redirect to
        return redirect()->route('service.inProgress');
    }

    public function onHold(Request $request)
    {
        if ($request->ajax()) {

            $services = Service::join('users', 'services.user_id', '=', 'users.id')
                ->select(['services.id', 'users.name', 'services.equipment', 'services.serial_no', 'services.note', 'services.created_at'])->where('status',1);
            return Datatables::of($services)
                ->addIndexColumn()
                ->editColumn('equipment', function($service){
                    return Str::limit($service->equipment, 30);
                })
                ->editColumn('note', function($service){
                    return Str::limit($service->note, 10);
                })
                ->editColumn('created_at', function ($service) {
                    return $service->created_at->format('Y/m/d');
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
                })
                ->addColumn('action', function($service){
                    $btn = '<a href="/service/onHold/serviceActive/'.$service->id.'" class="edit btn btn-outline-success btn-sm" style="margin-right: 4px;">Service</a>';
                    $btn = $btn.'<a href="/service/onHold/serviceFinished/'.$service->id.'" class="edit btn btn-outline-secondary btn-sm">Finished</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users.service.onHold');
    }

    public function onHoldActive($id){
        $service = Service::find($id);
        $service->update([
            'status' => 2
        ]);
        //Session message
        session()->flash('success', 'You push successfully on service');
        //Redirect to
        return redirect()->route('service.onHold');
    }


    public function onHoldFinished($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You successfully finished service');
        //Redirect to
        return redirect()->route('service.onHold');
    }

    public function finished(Request $request)
    {
        if ($request->ajax()) {
            $services = Service::join('users', 'services.user_id', '=', 'users.id')
                ->select(['services.id', 'users.name', 'services.equipment', 'services.serial_no', 'services.note', 'services.created_at','services.status'])->where('services.status',3)->orWhere('services.status',4);
            return Datatables::of($services)
                ->addIndexColumn()
                ->editColumn('equipment', function($service){
                    return Str::limit($service->equipment, 30);
                })
                ->editColumn('note', function($service){
                    return Str::limit($service->note, 10);
                })
                ->editColumn('created_at', function ($service) {
                    return $service->created_at->format('Y/m/d');
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
                })
                ->addColumn('action', function($service){
                    if( $service->status === 3 ){
                        $btn = '<a href="/service/serTaken/'.$service->id.'" class="edit btn btn-outline-warning btn-sm" style="margin-right: 4px;">Taken</a>';
                    }else{
                        $btn = '<a href="/service/notTaken/'.$service->id.'" class="edit btn btn-warning btn-sm">Taken</a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users.service.finished');
    }

    public function taken(Request $request)
    {
        if ($request->ajax()) {
            $services = Service::join('users', 'services.user_id', '=', 'users.id')
                ->select(['services.id', 'users.name', 'services.equipment', 'services.serial_no', 'services.note', 'services.created_at','services.updated_at'])->where('status',4);

            return Datatables::of($services)
                ->addIndexColumn()
                ->editColumn('equipment', function($service){
                    return Str::limit($service->equipment, 30);
                })
                ->editColumn('note', function($service){
                    return Str::limit($service->note, 10);
                })
                ->editColumn('created_at', function ($service) {
                    return $service->created_at->format('Y/m/d');
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
                })
                ->editColumn('updated_at', function ($service) {
                    return $service->updated_at->format('Y/m/d');
                })
                ->filterColumn('updated_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(updated_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
                })

                ->make(true);
        }
        return view('users.service.taken');
    }


    public function serTaken($id){
        $service = Service::find($id);
        $service->update([
            'status' => 4
        ]);
        //Session message
        session()->flash('success', 'You successfully taken service');
        //Redirect to
        return redirect()->route('service.finished');
    }

    public function serNotTaken($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You successfully restored service from taken');
        //Redirect to
        return redirect()->route('service.finished');
    }

    public function show($id){

        $service = Service::find($id);
        return view('users.service.show',compact('service'));
    }


   //Modal
    public function modalStore(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=> 'required|min:2|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' =>'required|string|min:6|max:15',

        ],[
            'name.required' => 'The name field is required',
            'name.min' => 'The name must be at list 2 character long'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->toArray()]);
        } else {

            $user = User::create([
                'name'=>$request->name,
                'email' => $request->email,
                'password' =>bcrypt($request->password) ,
            ]);

            if($user){
                return response()->json(['success'=>'New user has been successfully registered']);
            }

        }
    }



}


