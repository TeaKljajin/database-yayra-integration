-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2021 at 11:20 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `services`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pib` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `pib`, `street`, `number`, `city`, `contact_person`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(4, 'Lidl', '6554poiup', 'Maroko', '88', 'Maroko', 'John', 'jin@gmail.com', '34535359897', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2021_04_10_151321_create_companies_table', 2),
(8, '2021_04_10_201044_create_services_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `equipment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_no` int(11) DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `user_id`, `equipment`, `serial_no`, `note`, `status`, `created_at`, `updated_at`) VALUES
(13, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip', 54444, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 4, '2021-04-11 22:00:00', '2021-04-18 12:04:07'),
(14, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip', 87777, 'Lorem ipsum dolor sit amet,', 2, '2021-04-16 22:00:00', '2021-04-18 10:55:15'),
(15, 8, 'This article describes how to use personal storage folders, also known as .pst files, to back up data that you created in', 878788, 'This article describes how to', 1, '2021-04-13 22:00:00', NULL),
(16, 5, 'Outlook 2003, and Microsoft Office Outlook 2002. You can back up messages, contacts, appointments, tasks, notes, and journal entries in .pst files.', 9898, 'to use personal storage folders, also known as .pst files, to back up data', 3, '2021-04-16 22:00:00', '2021-04-18 14:11:06'),
(17, 46, 'from two different tables. In my module table Ive kept membership_id as key to maintain one to many relationship,', 5555, 'from two different tables. In my module table', 1, '2021-04-13 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tea', 'user@gmail.com', NULL, '$2y$10$DurWrlS6YYFbkJM1d72TL.weoQcliFd5FNUfHZfHYkEPm6/vdwm4a', NULL, NULL, NULL),
(2, 'sfsdf', 'sdfsr@gmail.com', NULL, '$2y$10$cptKWzsgAciCFblMgok5qu6pUwv4maw200ybcvYD1a8Z5VMBYTpfW', NULL, NULL, NULL),
(3, 'John', 'usertr@gmail.com', NULL, '$2y$10$87AemnixD9tVPpqH0PxlRuUymhiB0/aCRGVAPLxHpGEYou5/BaIg6', NULL, NULL, NULL),
(4, 'John', 'usergf@gmail.com', NULL, '$2y$10$3VDbssXM9pKPfoTw4krX9easHOMfBMhivreUX3A4Z/bH0OAeVG9R6', NULL, NULL, NULL),
(5, 'John', 'userf@gmail.com', NULL, '$2y$10$m.qa79Pru697xMh4.EyCGe68ntHwmu8zoKaubvENO6/qSTzDkRgUq', NULL, '2021-04-11 12:42:26', '2021-04-11 12:42:26'),
(6, 'rrr', 'userrrr@gmail.com', NULL, '$2y$10$ifVOAztIyF4lkFe0qZq5A.6mEi.EUkgtzAIEzVA1I2vUuweoQKAKy', NULL, '2021-04-11 12:42:48', '2021-04-11 12:42:48'),
(7, 'Tea Kljajin', 'tea4@gmail.com', NULL, '123456789', NULL, '2021-04-17 18:37:47', '2021-04-17 18:37:47'),
(8, 'Tea Kljajin', 'teakljajsssin@gmail.com', NULL, '123456789', NULL, '2021-04-17 18:38:13', '2021-04-17 18:38:13'),
(9, 'Tea Kljajin', 'teakljadddjsssin@gmail.com', NULL, '123456789', NULL, '2021-04-17 18:38:34', '2021-04-17 18:38:34'),
(10, 'Tea Kljajin', 'teakljajin7777@gmail.com', NULL, '123456789', NULL, '2021-04-17 18:39:51', '2021-04-17 18:39:51'),
(11, 'Tea Kljajin', 'teakljajsssin8888@gmail.com', NULL, '123456789', NULL, '2021-04-17 18:40:08', '2021-04-17 18:40:08'),
(12, 'John', 'teakljajsssssssin@gmail.com', NULL, '123456789', NULL, '2021-04-17 18:45:41', '2021-04-17 18:45:41'),
(13, 'John', 'tuun@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:39:15', '2021-04-18 05:39:15'),
(14, 'John', 'tuupn@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:39:23', '2021-04-18 05:39:23'),
(15, 'John', 'tuupniuuu@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:40:09', '2021-04-18 05:40:09'),
(16, 'oo', 'in@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:40:43', '2021-04-18 05:40:43'),
(17, 'mmm', 'ssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:44:00', '2021-04-18 05:44:00'),
(18, 'tt', 'hssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:44:57', '2021-04-18 05:44:57'),
(19, 'rr', 'sssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:46:09', '2021-04-18 05:46:09'),
(20, 'tt', 'sin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:48:32', '2021-04-18 05:48:32'),
(21, 'www', 'n@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:49:19', '2021-04-18 05:49:19'),
(22, 'rr', 'ssssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:49:53', '2021-04-18 05:49:53'),
(23, 'yy', 'ssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:51:06', '2021-04-18 05:51:06'),
(24, 'ddd', 'fsssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:53:46', '2021-04-18 05:53:46'),
(25, 'hh', 'hsssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:54:29', '2021-04-18 05:54:29'),
(26, 'sdfsdf', 'sdfssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:55:02', '2021-04-18 05:55:02'),
(27, 'sf', 'sdfssssssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:55:42', '2021-04-18 05:55:42'),
(28, 'dfgdf', 'uuusssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:56:41', '2021-04-18 05:56:41'),
(29, 'Tea Kljajin', 'xcvxsssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:57:58', '2021-04-18 05:57:58'),
(30, 'sdfsd', 'sdfsd@gmail.com', NULL, '123456789', NULL, '2021-04-18 05:59:16', '2021-04-18 05:59:16'),
(31, 'dd', 'uusssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 06:02:22', '2021-04-18 06:02:22'),
(32, 'dd', 'ddjsssssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 06:02:37', '2021-04-18 06:02:37'),
(33, 'ee', 'eeeajsssssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 06:03:41', '2021-04-18 06:03:41'),
(34, 'Tea Kljajin', 'sdskljajin@gmail.com', NULL, 'sdsdssddsssd', NULL, '2021-04-18 06:11:40', '2021-04-18 06:11:40'),
(35, 'nnn', 'sssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 06:13:35', '2021-04-18 06:13:35'),
(36, 'hllkhk', 'hhklsssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 06:14:27', '2021-04-18 06:14:27'),
(37, 'ghjkgh', 'gjgjgsssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 06:15:03', '2021-04-18 06:15:03'),
(38, 'rrrr', 'rrsssssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 06:15:42', '2021-04-18 06:15:42'),
(39, 'dsfs', 'sdfsssssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 06:16:25', '2021-04-18 06:16:25'),
(40, 'dfgdf', 'dfgjsssssssin@gmail.com', NULL, '123456789', NULL, '2021-04-18 06:17:22', '2021-04-18 06:17:22'),
(41, 'wr', 'ewrwssssssin@gmail.com', NULL, '123456789ewrw', NULL, '2021-04-18 06:19:28', '2021-04-18 06:19:28'),
(42, 'wwww', 'eein@gmail.com', NULL, '$2y$10$8lX00tGVnI1yc/.Fo6BPieZI9ul8rH9ze2PMCSJVq20cPLVwGxkjG', NULL, '2021-04-18 07:01:16', '2021-04-18 07:01:16'),
(43, 'dfg', 'fgfsssssin@gmail.com', NULL, '$2y$10$KEVP282isdgwE9eaDJi4cO3Ammq0QcDXdVsddC1t59xZuZG4UB7n2', NULL, '2021-04-18 07:01:50', '2021-04-18 07:01:50'),
(44, 'sdf', 'fdsjsssssssin@gmail.com', NULL, '$2y$10$SQxPLF3wiQfrn/f7ytaXWuJKPVu1EKoDT5mZPdfOoVJr4m2wqBpmi', NULL, '2021-04-18 07:03:49', '2021-04-18 07:03:49'),
(45, 'dsdf', 'sadsssssssin@gmail.com', NULL, '$2y$10$GYapLylvMtSYiH/0JlS4AuJoeFPFfhrc1ZhZdCw6vfG3mHq9UZQ0m', NULL, '2021-04-18 07:11:15', '2021-04-18 07:11:15'),
(46, 'Ivana', 'ivana@gmail.com', NULL, '$2y$10$uCrMdNNTSpELvf8AUe3Fp.zVIx43w5yCgdI7pEEP5VtM7feAmLw0e', NULL, '2021-04-18 14:13:05', '2021-04-18 14:13:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companies_company_name_unique` (`company_name`),
  ADD UNIQUE KEY `companies_pib_unique` (`pib`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
