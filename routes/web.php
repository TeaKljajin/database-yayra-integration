<?php


use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Front\PublicController;
use \App\Http\Controllers\Front\LoginController;
use \App\Http\Controllers\User\DashboardController;
use \App\Http\Controllers\User\CompanyController;
use \App\Http\Controllers\User\ServiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Front
Route::get('/',[PublicController::class,'index'])->name('main');
Route::get('/login',[LoginController::class,'login'])->name('login');
Route::post('/signIn',[LoginController::class,'signIn'])->name('signIn');
Route::get('/register',[LoginController::class,'register'])->name('register');
Route::post('/signUp',[LoginController::class,'signUp'])->name('signUp');



//User Dashboard
Route::get('/dashboard',[DashboardController::class,'index'])->name('dashboard');
Route::get('/logout',[DashboardController::class,'logout'])->name('logout');

//Company
Route::get('/company/create',[CompanyController::class,'create'])->name('company.create');
Route::post('/company/store',[CompanyController::class,'store'])->name('company.store');
Route::get('/company/edit/{id}',[CompanyController::class,'edit'])->name('company.edit');
Route::post('/company/update/{id}',[CompanyController::class,'update'])->name('company.update');
Route::get('/company/delete/{id}',[CompanyController::class,'delete'])->name('company.delete');

//Service
Route::get('/service/index',[ServiceController::class,'index'])->name('service.index');
Route::post('/service/store',[ServiceController::class,'store'])->name('service.store');
Route::post('/service/index/storeUser',[ServiceController::class,'modalUser'])->name('service.modalUser');


//Service/Accepted
Route::get('/service/accepted',[ServiceController::class,'accepted'])->name('service.accepted');
Route::get('/service/accepted/serviceActive/{id}',[ServiceController::class,'serviceActive'])->name('service.serviceActive');
Route::get('/service/accepted/serviceNotActive/{id}',[ServiceController::class,'serviceNotActive'])->name('service.serviceNotActive');
Route::get('/service/accepted/finished/{id}',[ServiceController::class,'serviceFinished'])->name('service.Finished');
Route::get('/service/accepted/notFinished/{id}',[ServiceController::class,'serviceNotFinished'])->name('service.notFinished');
Route::get('/service/accepted/taken/{id}',[ServiceController::class,'serviceTaken'])->name('service.serviceTaken');
Route::get('/service/accepted/notTaken/{id}',[ServiceController::class,'serviceNotTaken'])->name('service.serviceNotTaken');
Route::get('/service/accepted/delete/{id}',[ServiceController::class,'delete'])->name('service.delete');
Route::get('/service/accepted/show/{id}',[ServiceController::class,'show'])->name('service.show');



//Service/On Hold
Route::get('/service/onHold',[ServiceController::class,'onHold'])->name('service.onHold');
Route::get('/service/onHold/serviceActive/{id}',[ServiceController::class,'onHoldActive'])->name('service.onHoldActive');
Route::get('/service/onHold/serviceFinished/{id}',[ServiceController::class,'onHoldFinished'])->name('service.onHoldFinished');

//Service/In Progress
Route::get('/service/inProgress',[ServiceController::class,'inProgress'])->name('service.inProgress');
Route::get('/service/inProgress/serviceNotActive/{id}',[ServiceController::class,'inProgressNotActive'])->name('service.inProgressNotActive');
Route::get('/service/inProgress/serviceFinished/{id}',[ServiceController::class,'inProgressFinished'])->name('service.inProgressFinished');

//Service/Taken
Route::get('/service/taken',[ServiceController::class,'taken'])->name('service.taken');
Route::get('/service/serTaken/{id}',[ServiceController::class,'serTaken'])->name('service.serTaken');
Route::get('/service/notTaken/{id}',[ServiceController::class,'serNotTaken'])->name('service.serNotTaken');

Route::get('/service/finished',[ServiceController::class,'finished'])->name('service.finished');

//Modal
Route::post('/service/modalStore',[ServiceController::class,'modalStore'])->name('modalStore');

